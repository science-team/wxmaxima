Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wxMaxima
Upstream-Contact: Andrej Vodopivec <andrej.vodopivec@gmail.com>
Source: http://wxmaxima.sourceforge.net/

Files: *
Copyright: © 2004-2013 Andrej Vodopivec <andrej.vodopivec@gmail.com>
           © 2012-2013 Doug Ilijev <doug.ilijev@gmail.com>
           © 2011 cw.ahbong <cw.ahbong@gmail.com>
           © 2008-2009 Ziga Lenarcic <zigalenarcic@users.sourceforge.net>
License: GPL-2+

Files: art/*
Copyright: © Sven Hodapp (http://4pple.de)
License: GPL

Files: art/toolbar/*
Copyright: © Tango project (http://tango.freedesktop.org)
License: CC-BY-SA

Files: data/wxmath.lisp
Copyright: © 2004-2007 Andrej Vodopivec
           © 1987 Richard J. Fateman
License: GPL-2+

Files: locales/ca.po
Copyright: © 2010-2012 Innocent De Marchi <tangram.peces@gmail.com>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/cs.po
Copyright: © 2009 Josef Barák <josefbarak@seznam.cz>
           © 2010 Robert Marik <maarik@mendelu.cz>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/de.po
Copyright: © 2005 Harald Geyer <Harald.Geyer@gmx.at>
License: other
 This file is released to the public domain.

Files: locales/es.po
Copyright: © 2005 Eulogio Serradilla <eulogio.sr@terra.es>
           © 2005-2007 Antonio Ullan <aullan@unex.es>
           © 2009-2011 Mario Rodriguez <mario@edu.xunta.es>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/fr.po
Copyright: © 2004 Eric <ericdelevaux@gmail.com>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/gl.po
Copyright: © 2012 Mario Rodriguez <mario@edu.xunta.es>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/it.po
Copyright: © 2005-2011 Marco Ciampa <ciampix@libero.it>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/pl.po
Copyright: © 2008-2009 Rafal Topolnicki <rtopolnicki@o2.pl>
           © 2012 Ihor Rokach <rokach@tu.kielce.pl>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/pt_BR.po
Copyright: © 2006-2012 Eduardo M Kalinowski <ekalin@bol.com.br>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/ru.po
Copyright: © 2006-2007 Vadim V. Zhytnikov <vvzhy@netorn.ru>
           © 2007 Sergey Semerikov <cc@kpi.dp.ua>
           © 2008-2009 Alexey Beshenov <al@beshenov.ru>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/zh_CN.po
Copyright: © 2013 crickzhang1 <crickzhang1@gmail.com>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: locales/zh_TW.po
Copyright: © 2009 rabbit <rabbit@ctu.edu.tw>
           © 2010-2012 cw.ahbong <cwahbong@users.sourceforge.net>
License: GPL-2+
 This file is distributed under the same license as the wxMaxima package.

Files: debian/*
Copyright: © 2005-2007 Rafa Rodriguez Galvan <rafael.rodriguez@uca.es>
           © 2005-2007 Marco Presi (Zufus) <zufus@debian.org>
           © 2009-2014 Frank S. Thomas <fst@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL'.

License: CC-BY-SA
 https://creativecommons.org/licenses/by-sa/2.5/legalcode
